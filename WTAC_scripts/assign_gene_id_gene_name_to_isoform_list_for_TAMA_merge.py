#!/usr/bin/env python3
# 03/04/2020: Edits of WTAC's original script with user-defined input 
# Aim: convert column 4 of input_genome_bed12 with "gene ID: transcript ID" taken from gene_id_trascript_id_input_file 
# Input: 
	# gene_id_transcript_id_input_file: output file from list_mm10_gene_transcript.R, list of gene_id, transcript id, gene name 
	# original genome.bed12 file prior to formatting for tama merge 
# Output: original genome.bed12 file with only column 4 changed to be used as input for Tama_Merge 

##### Example of Running script 
#TAMA_REFERENCE=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/reference_2019/TAMA_REFERENCE
#python assign_gene_id_gene_name_to_isoform_list_for_TAMA_merge.py $TAMA_REFERENCE/gene_id_transcript_id_gene_name_mm10vM22_ERCC_SIRV $TAMA_REFERENCE/Tama_gencode.vM22.TAMA.annotation.bed12 $TAMA_REFERENCE/Tama_formatted_gencode.vM22.TAMA.annotation.bed12
#####


import sys

gene_id_transcript_id_input_file = sys.argv[1]
genome_of_interest_bed12_input_file = sys.argv[2]
output_file_path = sys.argv[3]

gene_id_transcript_id = open(gene_id_transcript_id_input_file,"r")
genome_of_interest_bed12 = open(genome_of_interest_bed12_input_file ,"r")

output_file = open(output_file_path,"w")

list_of_gene_id=[]
list_of_transcript_id=[]
list_of_gene_name=[]


line_of_input_file = gene_id_transcript_id.readline().rstrip()

while line_of_input_file:
	words = line_of_input_file.split("\t",3)
	list_of_gene_id.append(words[0])
	list_of_transcript_id.append(words[1])
	list_of_gene_name.append(words[2])
	line_of_input_file = gene_id_transcript_id.readline().rstrip()

gene_id_transcript_id.close()


line_of_input_file_2 = genome_of_interest_bed12.readline().rstrip()

while line_of_input_file_2:
	words = line_of_input_file_2.split("\t",12)
	transcript_id_read = words[3]
	position=list_of_transcript_id.index(transcript_id_read)
	gene_id_read=list_of_gene_id[position]
	gene_name_read=list_of_gene_name[position]
	output_file.write(str(words[0])+"\t"+str(words[1])+"\t"+str(words[2])+"\t"+
str(gene_id_read)+"_"+str(gene_name_read)+";"+str(transcript_id_read)+"\t"+str(words[4])+"\t"+str(words[5])+"\t"+str(words[6])+"\t"+str(words[7])+"\t"+str(words[8])+"\t"+str(words[9])+"\t"+str(words[10])+"\t"+str(words[11])+"\n")
	line_of_input_file_2 = genome_of_interest_bed12.readline().rstrip()

output_file.close()
