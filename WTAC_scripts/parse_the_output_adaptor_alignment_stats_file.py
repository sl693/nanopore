#!/usr/bin/env python3
# 03/04/2020: Edits of WTAC's original script (as error processing) with tim's smig_parse_wind.py with user-defined input 

import numpy
import sys

user_defined_input_file = sys.argv[1]
user_defined_output_file = sys.argv[2]

input_file = open(user_defined_input_file,"r")
output_file = open(user_defined_output_file,"w")
line = input_file.readline().rstrip()
words = line.split(" ",1)
first_word=words[0]
if first_word[:10]== "read_name:":
    full_read_name=words[0]
    
line = input_file.readline().rstrip()
while line:
    words = line.split(" ",1)
    first_word=words[0]

    if first_word[:10]== "read_name:":
        full_read_name=words[0].split(' ')[0]
        output_file.write(str(full_read_name)+"\t"+str(final_selected_start_type)+"\t"+str(final_selected_start_score)+"\t"+str(final_selected_end_type)+"\t"+str(final_selected_end_score)+"\n")

    if first_word== "adaptors_at_the_start:":
        final_selected_start_type=words[1].split(' ')[0][:-1]
        final_selected_start_score = words[1].split(' ')[2][6:-1]
            
    if first_word== "adaptors_at_the_end:":
        final_selected_end_type=words[1].split(' ')[0][:-1]
        final_selected_end_score = words[1].split(' ')[2][6:-1]
            
    line = input_file.readline().rstrip()

input_file.close()
output_file.close()