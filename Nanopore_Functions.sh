#!/bin/sh
#PBS -V # export all environment variables to the batch job.
#PBS -q mrcq # submit to the serial queue
#PBS -l walltime=144:00:00 # Maximum wall time for the job.
#PBS -A Research_Project-MRC148213
#PBS -l procs=1 # specify number of processors.
#PBS -m e -M sl693@exeter.ac.uk # email me at job completion

# Szi Kay Leung (sl693@exeter.ac.uk)
# Date: 13th June 2019
# Define Functions for Nanpore Analysis

: '
1) Download the fasta format reference genome sequence to which sequence reads will be mapped
2) Download the GFF format reference genome annotations to be used for the analysis
3) Use the pychopper software to select for likely full-length cDNA transcript sequences and orientate them so that the protein coding sequence is present on the forward cDNA strand
4) Create a genome index for read mapping using the minimap2 (Li (2018)) software
5) Map the pychopper-filtered sequence reads against the reference genome using minimap2; sort and index the mapping results with samtools (Li et al. (2009))
6) Prepare a GFF annotation from the mapped read BAM file using Pinfish spliced_bam2gff
7) Correct sequence consensus (using Racon Vaser et al. (2017)) for transcript clusters with the pinfish polish_clusters tool
8) Remap the polished and transcript clustered sequence reads against the reference genome using minimap2
9) Prepare a GFF annotation from the polished and clustered mapped transcript BAM file using pinfish spliced_bam2gff
10) Collapse redundant and partial transcript annotations using pinfish collapse_partials
11) Prepare a fasta file of non-redundant, polished and collapsed transcripts using gffread
12) Compare the de novo produced transcript annotations with the reference genome annotations using GFFcompare. This is used to identify novel gene isoforms
'

module load Miniconda2
source activate nanopore

REFERENCE=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/reference_2019
RAWDATA=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/ONT/rawdata
M21_WKD=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/ONT/M21
K18_WKD=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/ONT/K18
SOFTWARES=/gpfs/mrc0/projects/Research_Project-MRC148213/sl693/softwares

# run_merge $fastqID $Sample_Name
# run_merge fastq_runid_f4ce1a8c1b372183cfe209bf9f7f07ec81f7c1e1 M21
run_merge(){
    cd $RAWDATA
    echo "Merging following fastq files"
    FASTQ="$(ls *$1*)" #$1=rawfastqid
    echo $FASTQ
    if [ -f $2.merged.fastq ];then
        echo "$2.merged.fastq.file already exists; Merging not needed for sample $2"
    else
        cat $FASTQ > $2.merged.fastq
        echo "Merge of Sample $2 with RunID $1 successful"
    fi
}

# run_nanofilt $sample $WKD
# run_nanofilt M21 $M21_WKD
run_nanofilt(){
    if cat $RAWDATA/$1.merged.fastq | NanoFilt -q 7 > $2/$1.filtered.reads.fastq; then
        echo "Nanofilt for Sample $1 successful"
    else 
        echo "Nanofilt for Sample $1 failed"
    fi
}

# run_porechop $sample $WKD
# run_porechop M21 $M21_WKD

# modified adaptor.py and nanopore_read.py scripts as according to WTAC 
# cd /gpfs/mrc0/projects/Research_Project-MRC148213/sl693/softwares/Porechop/build/lib/porechop
# mv nanopore_read.py nanopore_read_original.py
# cp /gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Scripts/nanopore/nanopore_read.py .
# cd /gpfs/mrc0/projects/Research_Project-MRC148213/sl693/softwares/Porechop/porechop 
# mv nanopore_read.py nanopore_read_original.py
# mv adapters.py adapters_original.py
# cp /gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Scripts/nanopore/nanopore_read.py .
# cp /gpfs/mrc0/projects/Research_Project-MRC148213/sl693/Scripts/nanopore/adapters.py .

run_porechop(){
    echo "Processing Sample $1 for Porechop"
    if python $SOFTWARES/Porechop/porechop-runner.py -i $2/$1.filtered.reads.fastq \
    -o $2/$1.filtered.reads.choped.fq --format fastq \
    --verbosity 4 --threads 16 \
    --check_reads 1000 \
    --discard_middle \
    --end_size 100 \
    --min_trim_size 15 \
    --extra_end_trim 1 \
    --end_threshold 75 > $2/$1.output.adaptor.alignment.stat; then 
        echo "Processing Sample $1 for Porechop successful"
    else
        echo "Processing Sample $1 for Porechop failed"
    fi

    echo "Processing Sample $1 for Porechop Arrangment (WTAC)"
    grep -e "^read_name:" -e "^adaptors_at_the_start:" -e "^adaptors_at_the_end:" $1.output.adaptor.alignment.stat >$1.grep.output.adaptor.alignment.stat

}

# run_pychopper M21 $M21_WKD
# run_pychopper K18 $K18_WKD
run_pychopper(){
    cd $2 #WKD
    echo "Processing Sample $1 for Pychopper"
    echo "Using the following primer.fasta file"
    cat $REFERENCE/nanopore.primer.fasta

    # -u for unclassified reads output
    # $1.merged.fastq = input fastq
    # $1.FL.fastq = output.fastq 
    #$REFERENCE/primer.fasta
    cdna_classifier.py -b $REFERENCE/nanopore.primer.fasta -r $1.report.pdf -u $1.unclassified.fastq -S $1.stats.output -A $1.scores.output $RAWDATA/$1.merged.fastq $1.FL.fastq
}

# run_minimap2 M21 $M21_WKD
run_minimap2(){
    minimap2 --version
    samtools 

    echo "Processing Sample $1 for Minimap2 and sort"
    cd $2 #WKD    
    minimap2 -t 30 -ax splice -uf --secondary=no -C5 -O6,24 -B4 $REFERENCE/mm10.fa $2/$1.FL.fastq > $1.sam 2> $1.map.log
    samtools sort -O SAM $1.sam > $1.sorted.sam
}


